DROP DATABASE IF EXISTS test_spring;
CREATE DATABASE test_spring DEFAULT CHARACTER SET utf8;
USE test_spring;

#创建用户表
CREATE TABLE user (
  user_id   INT AUTO_INCREMENT PRIMARY KEY,
  user_name VARCHAR(30) NOT NULL ,
  user_password  VARCHAR(32),
  user_credits INT DEFAULT 0,
  last_visit datetime,
  last_ip  VARCHAR(23)
)ENGINE=InnoDB CHARSET=utf8;

#创建用户登录日志表
CREATE TABLE login_log (
  login_log_id  INT AUTO_INCREMENT PRIMARY KEY,
  user_id   INT,
  ip  VARCHAR(23),
  login_datetime datetime
)ENGINE=InnoDB CHARSET=utf8;

#插入初始化数据
INSERT INTO user (user_name,user_password)
VALUES('assad','1234');
COMMIT;