package site.assad.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import site.assad.domain.LoginLog;


/**
 * Author: Al-assad 余林颖
 * E-mail: yulinying_1994@outlook.com
 * Date: 2017/12/17 12:52
 * Description:
 */
@Repository
public class LoginLogDao {

    @Autowired
    public JdbcTemplate jdbcTemplate;

    //添加登陆日志记录
    public void addLoginLog(LoginLog log){
        final String sql = "INSERT INTO login_log(user_id,ip,login_datetime) " +
                "VALUES (?,?,?)";
        jdbcTemplate.update(sql,new Object[]{log.getUserId(),log.getIp(),log.getLoginDate()});
    }

}
