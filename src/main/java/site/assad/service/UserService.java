package site.assad.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import site.assad.dao.LoginLogDao;
import site.assad.dao.UserDao;
import site.assad.domain.LoginLog;
import site.assad.domain.User;

/**
 * Author: Al-assad 余林颖
 * E-mail: yulinying_1994@outlook.com
 * Date: 2017/12/18 12:03
 * Description: User 登陆服务，提供登陆验证等服务
 */
@Service
public class UserService {

    @Autowired
    private UserDao userDao;
    @Autowired
    private LoginLogDao loginLogDao;

    //验证用户密码
    public boolean checkUser(final String username,final String password){
        return userDao.getUserPassowrdMatchCount(username,password) > 0;
    }

    //根据用户名查询用户
    public User findUserByName(final String username){
        return userDao.findUserByName(username);
    }

    @Transactional
    //登陆成功后的操作：更新 user，更新 login_log
    public void loginSuccess(User user){

        user.setCredits(10 + user.getCredits());
        LoginLog log = new LoginLog();
        log.setUserId(user.getId());
        log.setIp(user.getLastIp());
        log.setLoginDate(user.getLastVisit());

        userDao.updateLoginInfo(user);
        loginLogDao.addLoginLog(log);
    }






}
