package site.assad.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import site.assad.domain.User;
import site.assad.service.UserService;
import site.assad.web.bean.LoginCommand;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.util.Date;

/**
 * Author: Al-assad 余林颖
 * E-mail: yulinying_1994@outlook.com
 * Date: 2017/12/18 12:16
 * Description: 登陆业务控制器
 */
@RestController  //标记为一个 Spring MVC 的Controller
public class LoginController {

    @Autowired
    private UserService userService;

    //响应 index.html 请求，并将其转发到 login 视图
    @RequestMapping(value="/login")
    public String loginPage(){
        return "login/login";
    }


    //响应 loginCheck.html 请求
    @RequestMapping(value="/loginCheck",method = RequestMethod.POST)
    public ModelAndView loginCheck(HttpServletRequest request, LoginCommand loginCommand ) throws UnsupportedEncodingException {
        //验证登陆用户密码
        boolean isValidUser = userService.checkUser(loginCommand.getUserName(),loginCommand.getPassword());

        //验证结果处理逻辑
        if(!isValidUser){
            //验证失败，返回 login 视图，并携带错误信息
            return new ModelAndView("login/login","error", new String("用户名或密码错误".getBytes(),"UTF-8"));
            //如果直接返回ModelAndView的对象携带的模型对象为 String ，由于服务器编码可能与代码编码不一致，可能会导致传输后字符乱码，这里强制将该 String 对象按按字节转化为 UTF-8 编码格式；
            //可以将该逻辑抽象为一个工具类，方便调用，这种解决方式是硬编码方式解决问题的，也可以通过设置服务器编码格式与代码编码，数据库编码统一来解决这个问题；
        }else{
            User user = userService.findUserByName(loginCommand.getUserName());
            user.setLastIp(request.getLocalAddr());
            user.setLastVisit(new Date());
            userService.loginSuccess(user);                         //执行验证成功的业务操作
            request.getSession().setAttribute("user",user);  //设置 session
            //验证成功，转发到 login_success 视图
            return new ModelAndView("login/login_success");
        }
    }

}
