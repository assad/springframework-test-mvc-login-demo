package site.assad.domain;

import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * Author: Al-assad 余林颖
 * E-mail: yulinying_1994@outlook.com
 * Date: 2017/12/17 12:47
 * Description:
 */
public class LoginLog {

    private int Loginid;
    private int userId;
    private String ip;
    private Date loginDate;

    public LoginLog() {
    }

    public int getLoginid() {
        return Loginid;
    }

    public void setLoginid(int loginid) {
        Loginid = loginid;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Date getLoginDate() {
        return loginDate;
    }

    public void setLoginDate(Date loginDate) {
        this.loginDate = loginDate;
    }
}
