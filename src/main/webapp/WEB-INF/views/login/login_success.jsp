<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Author: Al-assad 余林颖
  E-mail: yulinying_1994@outlook.com
  Date: 2017/12/18 12:22 
  Description:
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login Success Page</title>
</head>
<body>
    <h1>登陆成功！${user.name}， 您当前的积分为 ${user.credits} </h1>
</body>
</html>
