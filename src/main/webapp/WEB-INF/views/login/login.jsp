<%--
  Author: Al-assad 余林颖
  E-mail: yulinying_1994@outlook.com
  Date: 2017/12/18 12:21 
  Description: 登陆视图页面
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login Page</title>
</head>
<body>
<p>
    <c:if test="${!empty error}">
        <c:out value="${error}" />
    </c:if>
</p>

    <form action="<c:url value="/loginCheck" />" method="post">
        <label>用户名</label><input type="text" name="userName" /><br />
        <label>密码</label><input type="password" name="password"/><br />
        <input type="submit" value="登陆" />
        <input type="reset" value="重置信息" />
    </form>
</body>
</html>
